import { CourseExamplePage } from './app.po';

describe('course-example App', function() {
  let page: CourseExamplePage;

  beforeEach(() => {
    page = new CourseExamplePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
