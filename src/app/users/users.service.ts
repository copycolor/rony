import { Injectable } from '@angular/core';
import { Http }       from '@angular/http';
import {AngularFire} from 'angularfire2'; 
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/map';


@Injectable()
export class UsersService {
  //private _url = "http://jsonplaceholder.typicode.com/users";
  private _url = "http://ronih.myweb.jce.ac.il/distributed/api/users/";
  
  usersObservable;

  constructor(private af:AngularFire, private _http:Http ) { }
  
  addUser(user){
    this.usersObservable.push(user);
  }

  deleteUser(user){
    this.af.database.object('/users/' + user.$key).remove();
    console.log('/users/' + user.$key);
  }

  updateUser(user){
    let user1 = {name:user.name,email:user.email}
    console.log(user1);
    this.af.database.object('/users/' + user.$key).update(user1)
  }  

  getUsers(){
    this.usersObservable = this.af.database.list('/users').map(
      users =>{
        users.map(
          user => {
            user.posTitles = [];
            for(var p in user.posts){
                user.posTitles.push(
                this.af.database.object('/posts/' + p)
              )
            }
          }
        );
        return users;
      }
    )
    //this.usersObservable = this.af.database.list('/users');
    return this.usersObservable;
	}
  getUserFromRestApi(){
    let url = this._url;
    return this._http.get(url).map(res => res.json());
  }
}
